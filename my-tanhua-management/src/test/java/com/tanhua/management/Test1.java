package com.tanhua.management;

import com.tanhua.management.service.AnalysisService;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Random;

/**
 * @ClassName :  Test1
 * @Author : Tenebrous
 * @Date: 2020/12/25 22:54
 * @Description :
 */
public class Test1 {

    @Autowired
    private AnalysisService analysisService;

//    public static void main(String[] args) throws ParseException {
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        //Date转为时间戳long
//        String date = simpleDateFormat.format(System.currentTimeMillis()-(System.currentTimeMillis()/(3600*24*365)));
//        Long newDate = simpleDateFormat.parse(date).getTime();
//        long day = 3600*24;
//        for (int i = 0; i < 730; i++) {
//
//            System.out.println("insert into tb_analysis_by_day (record_date, num_registered, num_active, num_login, num_retention1d) " +
//                    "values (" + newDate + "," + new Random().nextInt(1000) + 1000 + "," + new Random().nextInt(1000) + 1000 +"," + new Random().nextInt(1000) + 1000 + "," + new Random().nextInt(1000) + 1000 + ");");
//            newDate += day;
//        }
//
//    }

    public static void main(String[] args) {
        String str = "餐饮行业, 服务行业, 教育行业, 计算机行业, 金融行业, 娱乐行业, 学生, 贸易, 设计";
        String[] split = str.split(",");
        for (int i = 0; i < 60; i++) {
            System.out.println("update tb_user_info set industry = " + "'" + split[new Random().nextInt(split.length)] + "'" + " where id = "+ i + ";");
        }
    }

}
