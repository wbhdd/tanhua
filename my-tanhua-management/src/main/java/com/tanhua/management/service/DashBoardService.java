package com.tanhua.management.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.management.mapper.LogMapper;
import com.tanhua.management.mapper.UserInfoMapper;
import com.tanhua.management.mapper.UsersMapper;
import com.tanhua.management.pojo.Log;
import com.tanhua.management.pojo.UserInfo;
import com.tanhua.management.vo.DateVo;
import com.tanhua.management.vo.InfoStatisticsVo;
import com.tanhua.management.vo.UsersStatisticsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class DashBoardService {

    @Autowired
    private UsersMapper usersMapper;

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Autowired
    private LogMapper logMapper;

    /**
     * 功能描述: 概要统计
     */
    public InfoStatisticsVo InfoStatistics() {
        //累计用户
        InfoStatisticsVo infoStatisticsVo = new InfoStatisticsVo();
        Integer integerCount = this.usersMapper.selectCount(null);
        infoStatisticsVo.setCumulativeUsers(integerCount);
        //过去30天活跃用户
        QueryWrapper<Log> monthQuery = new QueryWrapper<>();
        monthQuery.between("log_time", this.getMonthStartTime(), this.getTodayEndTime());
        Integer integerMonthLogin = this.logMapper.selectCount(monthQuery);
        infoStatisticsVo.setActivePassMonth(integerMonthLogin);
        //过去7天活跃用户
        QueryWrapper<Log> weekQuery = new QueryWrapper<>();
        weekQuery.between("log_time", this.getWeekStartTime(), this.getTodayEndTime());
        Integer integerWeekLogin = this.logMapper.selectCount(weekQuery);
        infoStatisticsVo.setActivePassWeek(integerWeekLogin);
        //今日新增用户
        List<UserInfo> userInfostoday = this.userInfoMapper.selectList(null);
        int todaycount = 0;
        for (UserInfo userInfo : userInfostoday) {
            Date created = userInfo.getCreated();
            long time = created.getTime();
            if (time >= this.getTodayStartTime() && time <= this.getTodayEndTime()) {
                todaycount++;
            }
        }
        infoStatisticsVo.setNewUsersToday(todaycount);

        //昨日新增用户
        List<UserInfo> userInfostomorrow = this.userInfoMapper.selectList(null);
        int tomorrowcount = 0;
        for (UserInfo userInfo : userInfostomorrow) {
            Date created = userInfo.getCreated();
            long time = created.getTime();
            if (time >= this.getTomorrowStartTime() && time <= this.getTomorrowEndTime()) {
                tomorrowcount++;
            }
        }
        //今日活跃用户/今日登录次数
        QueryWrapper<Log> todayQuery = new QueryWrapper<>();
        todayQuery.between("log_time", this.getTodayStartTime(), this.getTodayEndTime());
        Integer integerTodayLogin = this.logMapper.selectCount(todayQuery);
        infoStatisticsVo.setActiveUsersToday(integerTodayLogin);
        infoStatisticsVo.setLoginTimesToday(integerTodayLogin);
        //过去7天平均日使用时长，单位秒
        //昨日活跃用户/昨日登录次数
        QueryWrapper<Log> tomorrowQuery = new QueryWrapper<>();
        tomorrowQuery.between("log_time", this.getTomorrowStartTime(), this.getTomorrowEndTime());
        Integer integerTomorrowLogin = this.logMapper.selectCount(tomorrowQuery);
        infoStatisticsVo.setActiveUsersYesterday(integerTomorrowLogin);
        //前天活跃用户/前天登录次数
        QueryWrapper<Log> toToQuery = new QueryWrapper<>();
        toToQuery.between("log_time", this.getToToStartTime(), this.getToToEndTime());
        Integer integerToToLogin = this.logMapper.selectCount(toToQuery);
        //今日新增用户涨跌率，单位百分数，正数为涨，负数为跌
        int insert = 0;
        if (tomorrowcount == 0) {
            insert = (todaycount - tomorrowcount) / 1;
        } else {
            insert = (todaycount - tomorrowcount) / tomorrowcount * 1;
        }
        infoStatisticsVo.setNewUsersTodayRate(insert);
        //今日活跃用户涨跌率，单位百分数，正数为涨，负数为跌
        int up = 0;
        if (up == 0) {
            up = (integerTodayLogin - integerTomorrowLogin) / 1;
        } else {
            up = (integerTodayLogin - integerTomorrowLogin) / integerTomorrowLogin * 1;
        }
        infoStatisticsVo.setActiveUsersTodayRate(up);
        //昨日活跃用户涨跌率，单位百分数，正数为涨，负数为跌
        int tomorrowUp = 0;
        if (tomorrowUp == 0) {
            tomorrowUp = (integerToToLogin - integerTomorrowLogin) / 1;
        } else {
            tomorrowUp = (integerToToLogin - integerTomorrowLogin) / integerTomorrowLogin * 1;
        }
        infoStatisticsVo.setActiveUsersYesterdayRate(tomorrowUp);
        //今日登录次数涨跌率，单位百分数，正数为涨，负数为跌
        infoStatisticsVo.setLoginTimesTodayRate(up);
        return infoStatisticsVo;
    }

    private Long getTodayStartTime() {
        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        return todayStart.getTime().getTime();
    }

    private Long getTomorrowStartTime() {
        Calendar tomorrowStart = Calendar.getInstance();
        tomorrowStart.set(Calendar.HOUR_OF_DAY, -24);
        return tomorrowStart.getTime().getTime();
    }

    private Long getToToStartTime() {
        Calendar toToStart = Calendar.getInstance();
        toToStart.set(Calendar.HOUR_OF_DAY, -48);
        return toToStart.getTime().getTime();
    }

    private Long getWeekStartTime() {
        Calendar weekStart = Calendar.getInstance();
        weekStart.set(Calendar.HOUR_OF_DAY, -24 * 7);
        return weekStart.getTime().getTime();
    }


    private Long getMonthStartTime() {
        Calendar monthStart = Calendar.getInstance();
        monthStart.set(Calendar.HOUR_OF_DAY, -24 * 30);
        return monthStart.getTime().getTime();
    }

    /**
     * 获取今天结束时间
     */
    private Long getTodayEndTime() {
        Calendar todayEnd = Calendar.getInstance();
        todayEnd.set(Calendar.HOUR_OF_DAY, 23);
        todayEnd.set(Calendar.MINUTE, 59);
        todayEnd.set(Calendar.SECOND, 59);
        todayEnd.set(Calendar.MILLISECOND, 999);
        return todayEnd.getTime().getTime();
    }

    /**
     * 获取昨天结束时间
     */
    private Long getTomorrowEndTime() {
        Calendar tomorrowdayEnd = Calendar.getInstance();
        tomorrowdayEnd.set(Calendar.HOUR_OF_DAY, 0);
        tomorrowdayEnd.set(Calendar.MINUTE, 0);
        tomorrowdayEnd.set(Calendar.SECOND, 0);
        tomorrowdayEnd.set(Calendar.MILLISECOND, -1);
        return tomorrowdayEnd.getTime().getTime();
    }

    /**
     * 获取前天结束时间
     */
    private Long getToToEndTime() {
        Calendar toTodayEnd = Calendar.getInstance();
        toTodayEnd.set(Calendar.HOUR_OF_DAY, -24);
        toTodayEnd.set(Calendar.MINUTE, 0);
        toTodayEnd.set(Calendar.SECOND, 0);
        toTodayEnd.set(Calendar.MILLISECOND, -1);
        return toTodayEnd.getTime().getTime();
    }

    public UsersStatisticsVo getUsers(Long sd, Long ed, Integer type) {
        UsersStatisticsVo analysisUsersVo = new UsersStatisticsVo();
        List<DateVo> thisYear = new ArrayList<>();
        List<DateVo> lastYear = new ArrayList<>();
/**
 * 类型为101是新增
 */
        if (type == 101) {
            /**
             * 设置去年的起始时间
             */
            Calendar sdate = Calendar.getInstance();
            sdate.setTime(new Date(sd));
            sdate.add(Calendar.YEAR, -1);
            Date sdTime = sdate.getTime();
            Calendar edate = Calendar.getInstance();
            edate.setTime(new Date(ed));
            edate.add(Calendar.YEAR, -1);
            Date edTime = edate.getTime();
            //定义一个集合用来存储查询到符合时间区间的数据的月份
            List<Integer> months = new ArrayList<>();
            List<UserInfo> thislogs = this.userInfoMapper.selectList(null);
            //将符合条件的数据存储到新的集合中
            List<UserInfo> newThisLogs = new ArrayList<>();
            for (UserInfo thislog : thislogs) {
                long time = thislog.getCreated().getTime();
                if (time >= sd && time <= ed) {
                    newThisLogs.add(thislog);
                }
            }
            //将新的集合中的月份取出来存入到集合中
            for (UserInfo log : newThisLogs) {
                Date date = log.getCreated();
                int month = date.getMonth();
                months.add(month);

            }
            //一年12个月
            int s = 0;
            for (int i = 1; i <= 12; i++) {
                DateVo dateVo = new DateVo();
                //设置日期(即便利月份 一年12个月)
                dateVo.setTitle(String.valueOf((i)));
                //取得每个月的数据数量
                for (Integer month : months) {
                    if ((month + 1) == i) {
                        s++;
                    }
                }
                dateVo.setAmount((long) s);
                thisYear.add(dateVo);
                //将s重置为0以用来取下个月份的数据数量
                s = 0;
            }
            List<Integer> monthe = new ArrayList<>();
            List<UserInfo> lastlogs = this.userInfoMapper.selectList(null);
            List<UserInfo> newLastLogs = new ArrayList<>();
            for (UserInfo lastlog : lastlogs) {
                long time = lastlog.getCreated().getTime();
                if (time >= sdTime.getTime() && time <= edTime.getTime()) {
                    newLastLogs.add(lastlog);
                }
            }
            for (UserInfo log : newLastLogs) {
                Date date = log.getCreated();
                int month = date.getMonth();
                monthe.add(month);
            }
            int e = 0;
            for (int i = 1; i <= 12; i++) {
                DateVo dateVo = new DateVo();
                dateVo.setTitle(String.valueOf((i)));
                for (Integer month : monthe) {
                    if ((month + 1) == i) {
                        e++;
                    }
                }
                dateVo.setAmount((long) e);
                lastYear.add(dateVo);
                e = 0;
            }
        }
        if (type == 102) {
            QueryWrapper<Log> thisqueryWrapper = new QueryWrapper<>();
            Calendar sdate = Calendar.getInstance();
            sdate.setTime(new Date(sd));
            sdate.add(Calendar.YEAR, -1);
            Date sdTime = sdate.getTime();
            Calendar edate = Calendar.getInstance();
            edate.setTime(new Date(ed));
            edate.add(Calendar.YEAR, -1);
            Date edTime = edate.getTime();
            thisqueryWrapper.between("log_time", sd, ed);
            List<Integer> months = new ArrayList<>();
            List<Log> thislogs = this.logMapper.selectList(thisqueryWrapper);
            for (Log log : thislogs) {
                Date date = new Date(log.getLogTime());
                int month = date.getMonth();
                months.add(month);

            }
            int s = 0;
            for (int i = 1; i <= 12; i++) {
                DateVo dateVo = new DateVo();

                dateVo.setTitle(String.valueOf((i)));
                for (Integer month : months) {
                    if ((month + 1) == i) {
                        s++;
                    }
                }
                dateVo.setAmount((long) s);
                thisYear.add(dateVo);
                s = 0;
            }
            QueryWrapper<Log> lastqueryWrapper = new QueryWrapper<>();
            lastqueryWrapper.between("log_time", sdTime.getTime(), edTime.getTime());
            List<Integer> monthe = new ArrayList<>();
            List<Log> lastlogs = this.logMapper.selectList(lastqueryWrapper);
            for (Log log : lastlogs) {
                Date date = new Date(log.getLogTime());
                int month = date.getMonth();
                monthe.add(month);
            }
            int e = 0;
            for (int i = 1; i <= 12; i++) {
                DateVo dateVo = new DateVo();
                dateVo.setTitle(String.valueOf((i)));
                for (Integer month : monthe) {
                    if ((month + 1) == i) {
                        e++;
                    }
                }
                dateVo.setAmount((long) e);
                lastYear.add(dateVo);
                e = 0;
            }

        }
        if (type == 103) {

        }
        analysisUsersVo.setThisYear(thisYear);
        analysisUsersVo.setLastYear(lastYear);
        return analysisUsersVo;
    }
}
