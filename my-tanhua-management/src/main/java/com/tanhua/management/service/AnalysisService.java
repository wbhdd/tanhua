package com.tanhua.management.service;

import cn.hutool.core.date.DateUtil;
import com.tanhua.management.mapper.UserInfoMapper;
import com.tanhua.management.vo.AnalysisDistributionVo;
import com.tanhua.management.vo.DataPointVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName :  AnalysisService
 * @Author : Tenebrous
 * @Date: 2020/12/26 1:05
 * @Description :
 */
@Service
public class AnalysisService {

    @Autowired
    private UserInfoMapper userInfoMapper;

    public AnalysisDistributionVo queryUserDistribution(Long sd, Long ed) {

        AnalysisDistributionVo analysisDistributionVo = new AnalysisDistributionVo();

        //行业数据
        List<DataPointVo> industryDistribution = this.userInfoMapper.findIndustryDistribution();
        analysisDistributionVo.setIndustryDistribution(industryDistribution);

        // 性别
        List<DataPointVo> sexDistribution = this.userInfoMapper.findSexDistribution();
        for (DataPointVo dataPointVo : sexDistribution) {
            if ("1".equals(dataPointVo.getTitle())) {
                dataPointVo.setTitle("男");
            } else {
                dataPointVo.setTitle("女");
            }
        }
        analysisDistributionVo.setGenderDistribution(sexDistribution);

        // 年龄


        return analysisDistributionVo;
    }
}
