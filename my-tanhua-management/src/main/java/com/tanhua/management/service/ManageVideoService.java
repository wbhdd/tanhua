package com.tanhua.management.service;


import com.alibaba.dubbo.config.annotation.Reference;
import com.tanhua.dubbo.server.api.ManageVideoApi;
import com.tanhua.dubbo.server.pojo.Video;
import com.tanhua.dubbo.server.vo.PageInfo;
import com.tanhua.management.vo.PageResult;
import com.tanhua.management.vo.VideoVo;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ManageVideoService {


    @Reference(version = "1.0.0")
    private ManageVideoApi manageVideoApi;



    public PageResult queryVideoListById(Integer page, Integer pageSize, Long uid, String sortOrder, String sortProp) {
        //定义PageResult对象,最终返回前台
        PageResult pageResult = new PageResult();
        //设置分页参数
        pageResult.setPage(page);
        pageResult.setPageSize(pageSize);
        //获取dubbo的mongodb查询,返回一个pageinfo对象,
        PageInfo<Video> pageInfo = null;
        pageInfo = manageVideoApi.queryVideoListById(uid,page,pageSize,sortProp,sortOrder);
        //获取查询的列表数据
        List<Video> records = pageInfo.getRecords();
        //定义返回前台的items集合,将records中的数据放入items
        List<VideoVo> videoVoList = new ArrayList<>();
        for (Video record : records) {
            VideoVo videoVo = new VideoVo();

            videoVo.setUserId(record.getUserId());
            videoVo.setPicUrl(record.getPicUrl());
            videoVo.setCreateDate(record.getCreated());
            videoVo.setVideoUrl(record.getVideoUrl());
            videoVo.setId(record.getId().toHexString());
            //随机生成
            videoVo.setReportCount(RandomUtils.nextInt(50,80));
            videoVo.setLikeCount(RandomUtils.nextInt(50,500));//点赞数量
            videoVo.setCommentCount(RandomUtils.nextInt(0,50));
            videoVo.setForwardingCount(RandomUtils.nextInt(0,50));

            videoVoList.add(videoVo);
        }
        //装入总数
        pageResult.setCounts(pageInfo.getTotal());

        pageResult.setItems(videoVoList);


        return pageResult;
    }
}
