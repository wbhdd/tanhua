package com.tanhua.management.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tanhua.management.mapper.AdminMapper;
import com.tanhua.management.pojo.Admin;
import com.tanhua.management.vo.AdminVo;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName :  AdminService
 * @Author : Tenebrous
 * @Date: 2020/12/22 21:21
 * @Description :
 */
@Service
public class AdminService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private static final String redisKey = "CACHE_KEY_TOKEN_LOGIN";

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminService.class);

    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    private AdminMapper adminMapper;

    /**
     * 验证码
     */
    private static final String CACHE_CODE_PREFIX = "MANAGE_CAP_";

    public void saveVerification(String uuid,String code) {
        //将验证码存入redis,设置有效时间为3分钟
        String key= CACHE_CODE_PREFIX + uuid;
        this.redisTemplate.opsForValue().set(key,code, Duration.ofMinutes(3));
    }

    /**
     * @param username
     * @param password
     * @param code
     * @param uuid
     * @return
     */
    public String login(String username, String password, String code, String uuid) {
        if (StringUtils.isEmpty(username)) {
            return null;
        }
        if (StringUtils.isEmpty(password)) {
            return null;
        }
        QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", username);
        Admin admin = this.adminMapper.selectOne(queryWrapper);
        if (null == admin) {
            return null;
        }
        String pwd = DigestUtils.md5Hex(password);
        System.out.println(pwd);
        if (!StringUtils.equals(pwd, admin.getPassword())) {
            return null;
        }

        String redisKey = CACHE_CODE_PREFIX + uuid;

        String value = this.redisTemplate.opsForValue().get(redisKey);
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        if (!StringUtils.equalsIgnoreCase(value, code)) {
            // 如果从redis中取出的验证码与用户输入的不一致，验证码输入错误
            return null;
        }
        // 验证码正确,删除之前的验证码
        this.redisTemplate.delete(redisKey);

        // 将账号和用户id封装成map集合，然后生成token
        Map<String, Object> claims = new HashMap<String, Object>();
        claims.put("username", admin.getUsername());
        claims.put("id", admin.getId());

        // 生成token
        String token = Jwts.builder()
                //设置响应数据体
                .setClaims(claims)
                //设置加密方法和加密盐
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
        try {
            // 将token存储到redis中
            String redisTokenKey = "CACHE_KEY_TOKEN_LOGIN" + token;

            //将密码设置为null，不参与序列化
            admin.setPassword(null);

            String redisTokenValue = MAPPER.writeValueAsString(admin);
            this.redisTemplate.opsForValue().set(redisTokenKey, redisTokenValue, Duration.ofDays(7));
            return token;
        } catch (Exception e) {
            LOGGER.error("存储token出错", e);
            return null;
        }
    }

    /**
     * 用户基本信息
     *
     * @param token
     * @return
     */
    public AdminVo queryAdminByToken(String token) {
        try {
            String redisTokenKey = redisKey + token;
            String cacheData = redisTemplate.opsForValue().get(redisTokenKey);
            if (StringUtils.isEmpty(cacheData)) {
                return null;
            }
            redisTemplate.expire(redisTokenKey, 1, TimeUnit.HOURS);
            Admin admin = MAPPER.readValue(cacheData, Admin.class);
            System.out.println(admin);
            AdminVo adminVo = new AdminVo();
            adminVo.setUid(String.valueOf(admin.getId()));
            adminVo.setUsername(admin.getUsername());
            adminVo.setAvatar(admin.getAvatar());
            return adminVo;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void removeToken(String token) {
        String redisTokenKey = redisKey + token;
        this.redisTemplate.delete(redisTokenKey);
    }
}
