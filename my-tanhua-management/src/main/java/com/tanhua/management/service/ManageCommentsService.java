package com.tanhua.management.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.dubbo.server.api.ManageMomentsApi;
import com.tanhua.dubbo.server.pojo.Comment;
import com.tanhua.dubbo.server.pojo.Publish;
import com.tanhua.dubbo.server.vo.PageInfo;
import com.tanhua.management.mapper.UserInfoMapper;
import com.tanhua.management.pojo.UserInfo;
import com.tanhua.management.vo.CommentsVo;
import com.tanhua.management.vo.MomentsVo;
import com.tanhua.management.vo.PageResult;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class ManageCommentsService {


    @Reference(version = "1.0.0")
    private ManageMomentsApi manageCommentsApi;

    @Autowired
    private UserInfoMapper userInfoMapper;



    public PageResult queryMomentsListById(Long uid, Integer page, Integer pageSize, String sortProp, String sortOrder) {
        PageResult pageResult = new PageResult();
        pageResult.setPage(page);
        pageResult.setPageSize(pageSize);
        PageInfo<Publish> pageInfo = null;
        pageInfo = this.manageCommentsApi.queryMomentsListById(uid, page, pageSize, sortProp, sortOrder);
        List<Publish> records = pageInfo.getRecords();
        List<MomentsVo> momentsVos = new ArrayList<>();
        for (Publish record : records) {
            MomentsVo momentsVo = new MomentsVo();
            momentsVo.setUserId(record.getUserId());
            List<String> medias = record.getMedias();
            momentsVo.setCreateDate(record.getCreated());
            momentsVo.setMedias(record.getMedias());
            momentsVo.setText(record.getText());
            momentsVo.setId(record.getId().toHexString());
            momentsVo.setReportCount(RandomUtils.nextInt(50,80));
            momentsVo.setLikeCount(RandomUtils.nextInt(50,500));//点赞数量
            momentsVo.setCommentCount(RandomUtils.nextInt(0,50));
            momentsVo.setForwardingCount(RandomUtils.nextInt(0,50));


            momentsVos.add(momentsVo);
        }
        pageResult.setCounts(pageInfo.getTotal());


        pageResult.setItems(momentsVos);

        return pageResult;
    }




    public PageResult queryCommentsListById(String messageID, Integer page, Integer pageSize, String sortProp, String sortOrder){
        PageResult pageResult = new PageResult();
        pageResult.setPage(page);
        pageResult.setPageSize(pageSize);
        PageInfo<Comment> pageInfo = null;
        pageInfo = this.manageCommentsApi.queryCommentsListById(messageID, page, pageSize, sortProp, sortOrder);
        List<Comment> records = pageInfo.getRecords();
        List<CommentsVo> commentsVoList = new ArrayList<>();
        for (Comment record : records) {
            CommentsVo commentsVo = new CommentsVo();
            commentsVo.setId(record.getId().toHexString());
            QueryWrapper<UserInfo> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("user_id",record.getUserId());
            UserInfo userInfo = userInfoMapper.selectOne(queryWrapper);
            commentsVo.setNickname(userInfo.getNickName());
            commentsVo.setContent(record.getContent());
            commentsVo.setUserId(record.getUserId());
            commentsVo.setCreateDate(record.getCreated().toString());


            commentsVoList.add(commentsVo);


        }
        pageResult.setItems(commentsVoList);
        return pageResult;
    }

}
