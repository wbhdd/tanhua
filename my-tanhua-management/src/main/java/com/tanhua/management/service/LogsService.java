package com.tanhua.management.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tanhua.management.mapper.LogsMapper;
import com.tanhua.management.pojo.Logs;
import com.tanhua.management.vo.LogsVo;
import com.tanhua.management.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * @author zwy
 */
@Service
public class LogsService extends ServiceImpl<LogsMapper, Logs> {

    @Autowired
    private LogsMapper logsMapper;

    /**
     * 功能描述: getLogs   获取日志
     *
     * @param page
     * @param pageSize
     * @param uid
     * @param sortOrder
     * @param sortOrder1
     * @Return: {@link PageResult}
     * @throws:
     * @Author: zwy
     * @Date: 2020/12/25 22:45
     */
    public PageResult getLogs(Integer page, Integer pageSize, Long uid, String sortOrder, String sortOrder1) {
        PageResult pageResult = new PageResult();
        pageResult.setPage(page);
        pageResult.setPageSize(pageSize);

        QueryWrapper<Logs> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", uid);
        Integer count = this.logsMapper.selectCount(queryWrapper);

        IPage<Logs> logsIPage = new Page<>(page, pageSize);
        logsIPage = this.logsMapper.selectPage(logsIPage, queryWrapper);

        List<Logs> records = logsIPage.getRecords();
        List<LogsVo> newPages = new ArrayList<>();
        for (Logs record : records) {
            LogsVo logsVo = new LogsVo();
            logsVo.setId(record.getId());
            logsVo.setUserId(uid);
            logsVo.setLogTime(record.getLogTime());
            logsVo.setPlace(record.getPlace());
            logsVo.setEquipment(record.getEquipment());
            newPages.add(logsVo);
        }
        pageResult.setCounts(count);
        pageResult.setItems(newPages);
        return pageResult;
    }
}
