package com.tanhua.management.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.dubbo.server.api.UserLikeApi;
import com.tanhua.management.mapper.LogMapper;
import com.tanhua.management.mapper.UserInfoMapper;
import com.tanhua.management.pojo.Log;
import com.tanhua.management.pojo.User;
import com.tanhua.management.pojo.UserInfo;
import com.tanhua.management.vo.UserVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tanhua.management.mapper.UsersMapper;
import com.tanhua.management.vo.PageResult;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.Random;

/**
 * @ClassName :  UserService
 * @Author : Tenebrous
 * @Date: 2020/12/24 14:22
 * @Description :
 */
@Service
public class UsersService {

    private static final String CACHE_KEY_FREEZE_PREFIX = "FREEZE_";

    @Reference(version = "1.0.0")
    private UserLikeApi userLikeApi;

    @Autowired
    private UsersMapper userMapper;

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Autowired
    private LogMapper logMapper;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    public UserVo queryUserInfo(Long userId) {
        try {

            UserVo userVo = new UserVo();
            // 1、 查询 用户详情
            UserInfo userInfo = this.userInfoMapper.selectById(userId);
            if (null == userInfo) {
                return null;
            }
            userVo.setId(userId);
            userVo.setNickname(userInfo.getNickName());
            userVo.setSex(StringUtils.equals(userInfo.getSex() + "", "1") ? "男" : "女");
            userVo.setAge(userInfo.getAge());
            userVo.setIncome(Long.parseLong(userInfo.getIncome()) * 10000);
            userVo.setCity(userInfo.getCity());
            userVo.setTags(userInfo.getTags());
            userVo.setLogo(userInfo.getLogo());
            userVo.setOccupation(userInfo.getIndustry());

            //粉丝数、喜欢数、配对数
            userVo.setCountLiked(this.userLikeApi.queryLikeCount(userId));
            userVo.setCountBeLiked(this.userLikeApi.queryFanCount(userId));
            userVo.setCountMatching(this.userLikeApi.queryEachLikeCount(userId));

            String cacheKey = CACHE_KEY_FREEZE_PREFIX + userId;
            String userStatus = this.redisTemplate.opsForValue().get(cacheKey);
            if (null == userStatus) {
                userVo.setUserStatus("1");
            } else {
                userVo.setUserStatus("2");
            }
            // 获取手机号
            User user = this.userMapper.selectById(userId);
            if (null != user) {
                userVo.setMobile(user.getMobile());

                //指定转化前的格式
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                System.out.println(user.getCreated());
                //Date转为时间戳long
                String date = simpleDateFormat.format(user.getCreated().getTime());
                Long newDate = simpleDateFormat.parse(date).getTime();
                userVo.setCreated(newDate);
//                String str = "售货员、农民、售票员、空姐、邮差、设计师、发型师、搬运工、服务员、翻译、导游、咨客、秘书、文员、厨师、司机、歌手、演员、摄影师、模特、律师、法官、法律顾问、消防员、警察、交通协警、刑警、保安、出纳、会计师、审计员、职业经理人、股评家、证券经纪、广告从业员、保险从业员、美容顾问、裁缝、物管员、 清洁工、家教、保姆、护士、医生、法医、教师、整容师、化妆师、记者、主持人";
//                String[] occupations = str.split("、");

            } else {
                userVo.setMobile("保密");
            }
            String personalSignatures = "我所认为最深沉的爱，莫过于分开以后，我将自己，活成了你的样子、我还是很喜欢你，像风走了八千里，不问归期、少女的征途是星辰大海，而非烟火人间、天气晴朗，万物可爱，人间值得，未来可期、希望你别像风，再我这里掀起万般波澜，又随云而去、天上繁星，众星捧月，亦使我至死不渝、本来就不是人间星光，你谈何星辰大海、我当然不会试图摘月，我要月亮奔我而来、纵使你坠入悬崖，万劫不复，小行星仍正常运转、你说，星星睡不着会不会数人类呐、今天我上幼儿园没换鞋，他们都叫我小笨蛋，哼，我要用小本本把他们都记下来、你给我一滴眼泪，我就看到了你心中全部的海洋、初闻不知曲中意，再听已是曲中、一曲一歌，一醉一醒，一梦一生，也许，人生就是如此而已、云散月明谁点缀？天容海色本澄清、是很喜欢你，像日月轮回交-替，不理朝夕、我还是很喜欢你，像云漂泊九万里，不曾歇、既然错过星星，就别再错过月亮、愿天上的每一个流星，都为你闪耀天际、云朵偷喝了我藏在屋顶的酒，于是她的脸变成了晚、珍藏猫与深巷、人说，林深时见鹿，海蓝时见鲸，梦醒时见你。可林深时鹿逃，海蓝时浪涌，哪提梦醒时、你也会是仅属于一人限量贩售的快乐、月亮被嚼碎变成星星，你藏在漫天星光里、日落星光，冬去春来，尘埃落、他惊鸿一笑胜过所有、我是觉得你像雨后彩虹，突然出现，却让人很心动、本是一张白纸，哪会变成一沓情书、现在交通这么发达，越不过去的哪是山呐、所有的风都来拥抱你，你还盼着那片云、你看那些星星，都是我排位掉下来的、今晚月色很美，风也温柔、小太阳很忙，小云朵想吃糖、希望你别像云，在我这儿掀起万般波澜又随风而去、愿温柔的晚风吹散所有的不欢而散";
            String[] split = personalSignatures.split("、");
            userVo.setPersonalSignature(split[new Random().nextInt(split.length)]);
            Log log = logMapper.selectById(userInfo.getId());
            if(null != log){
                userVo.setLastActiveTime(log.getLogTime());
            } else {
                userVo.setLastActiveTime(userInfo.getCreated().getTime());
            }
            String city = "北京市、广东省、山东省、江苏省、河南省、上海市、河北省、浙江省、香港特别行政区、陕西省、湖南省、重庆市、福建省、天津市、云南省、四川省、广西壮族自治区、安徽省、海南省、江西省、湖北省、山西省、辽宁省、台湾省、黑龙江、内蒙古自治区、澳门特别行政区、贵州省、甘肃省、青海省、新疆维吾尔自治区、西藏省、吉林省、宁夏回族自治区";
            String[] cities = city.split("、");
            userVo.setLastLoginLocation(cities[new Random().nextInt(cities.length)]);
            System.out.println(userVo);
            return userVo;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public PageResult queryBypage(Integer page, Integer pageSize, Long id, String nickname, String city) throws ParseException {
        //创建分页对象设置参数
        PageResult pageResult = new PageResult();
        pageResult.setPage(page);
        pageResult.setPageSize(pageSize);
        //设置查模糊询条件
        QueryWrapper<UserInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(null != id, "user_id", id);
        queryWrapper.like(StringUtils.isNotBlank(nickname), "nick_name",nickname );
        queryWrapper.like(StringUtils.isNotBlank(city),"city", city);
        //查询数据的数量
        Integer count = userInfoMapper.selectCount(queryWrapper);
        //进行分页查询
        IPage<UserInfo> userPages = new Page<>(page, pageSize);
        userPages = userInfoMapper.selectPage(userPages, queryWrapper);
        //将查询到的数据保存到集合中
        List<UserInfo> userPages1 = userPages.getRecords();
        List<UserVo> newPages = new ArrayList<>();
        //将集合中的数据存入到返回给页面的集合中
        for (UserInfo userInfo : userPages1) {
            UserVo uservo = new UserVo();
            uservo.setId(userInfo.getId());
            uservo.setNickname(userInfo.getNickName());
            uservo.setCity(userInfo.getCity());
            uservo.setSex(userInfo.getSex().toString());
            String cacheKey = CACHE_KEY_FREEZE_PREFIX + userInfo.getUserId();
            String userStatus = this.redisTemplate.opsForValue().get(cacheKey);
            if (null == userStatus) {
                uservo.setUserStatus("1");
            } else {
                uservo.setUserStatus("2");
            }
            //通过用户id查询该用户的最近活跃时间(最后登录时间)
            Log log = logMapper.selectById(userInfo.getId());
            //如果没有查到最后登录时间的数据(即注册后为登录)则使用注册时间为最后活跃时间
            if(null != log){
                uservo.setLastActiveTime(log.getLogTime());
            } else {
                uservo.setLastActiveTime(userInfo.getCreated().getTime());
            }
            newPages.add(uservo);
        }
        pageResult.setCounts(count);
        pageResult.setItems(newPages);
        return pageResult;
    }
}
