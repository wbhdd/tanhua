package com.tanhua.management.exception;

/**
 * @author XDY
 * @date 2020/12/24
 */
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public BusinessException(String message) {
        super(message);
    }
}
