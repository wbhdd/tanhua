package com.tanhua.management.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @ClassName :  User
 * @Author : Tenebrous
 * @Date: 2020/12/10 20:13
 * @Description : 用户实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User extends BasePojo {

    private Long id;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 密码，json序列化时忽略
     */
    @JsonIgnore
    private String password;

    private Date created;

}
