package com.tanhua.management.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @ClassName :  User
 * @Author : Tenebrous
 * @Date: 2020/12/22 21:38
 * @Description : 管理员信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Admin extends BasePojo implements Serializable {

    /**
     * id
     */
    private Long id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    @JsonIgnore
    private String password;
    /**
     * 头像
     */
    private String avatar;


}
