package com.tanhua.management.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName :  BasePojo
 * @Author : Tenebrous
 * @Date: 2020/12/22 21:39
 * @Description :
 */
@Data
public abstract class BasePojo {

    /**
     * 自动填充
     */
    @TableField(fill = FieldFill.INSERT)
    private Date created;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updated;

}
