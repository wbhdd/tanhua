package com.tanhua.management.controller;

import com.tanhua.dubbo.server.pojo.Log;
import com.tanhua.management.service.LogsService;
import com.tanhua.management.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author zwy
 */
@Component
@RequestMapping("/manage/logs")
public class LogsController {
    @Autowired
    private LogsService logsService;

    /**
     * 功能描述: getLogs   查询日志
     *
     * @param
     * @Return: {@link ResponseEntity< List< Log>>}
     * @throws:
     * @Author: zwy
     * @Date: 2020/12/24 18:56
     */
    @GetMapping
    private ResponseEntity<PageResult> getLogs(@RequestParam(name = "page") Integer page,
                                               @RequestParam(value = "pagesize") Integer pageSize,
                                               @RequestParam(name = "uid") Long uid,
                                               @RequestParam(name = "sortProp") String sortProp,
                                               @RequestParam(name = "sortOrder") String sortOrder) {

        try {
            if (page <= 0) {
                page = 1;
            }
            PageResult pageResult = this.logsService.getLogs(page, pageSize, uid, sortProp, sortOrder);
            if (pageResult != null) {
                return ResponseEntity.ok(pageResult);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

}