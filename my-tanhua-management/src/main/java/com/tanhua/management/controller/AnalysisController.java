package com.tanhua.management.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tanhua.management.service.AnalysisService;
import com.tanhua.management.vo.AnalysisDistributionVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName :  AnalsisyController
 * @Author : Tenebrous
 * @Date: 2020/12/26 1:03
 * @Description :
 */
@RestController
@RequestMapping("dashboard")
public class AnalysisController {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    private AnalysisService analysisService;

    /**
     * 注册用户分布，行业top、年龄、性别、地区
     *
     * @param sd 开始时间
     * @param ed   结束时间
     * @return 注册用户分布，行业top、年龄、性别、地区
     */
    @GetMapping("distribution")
    public ResponseEntity<Object> queryUserDistribution(@RequestHeader(name = "Authorization") String token,
                                                        @RequestParam(name = "sd") Long sd,
                                                        @RequestParam("ed") Long ed) {

        AnalysisDistributionVo analysisDistributionVo = this.analysisService.queryUserDistribution(sd, ed);
        try {
            String json = MAPPER.writeValueAsString(analysisDistributionVo);
            return ResponseEntity.ok(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

}
