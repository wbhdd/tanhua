package com.tanhua.management.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tanhua.management.service.UsersService;
import com.tanhua.management.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.tanhua.management.vo.PageResult;
import com.tanhua.management.service.UserFreezeService;
import com.tanhua.management.vo.UserFreezeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;

/**
 * @author XDY
 * @date 2020/12/24
 */
@RestController
@RequestMapping("/manage/users")
public class UsersController {

    @Autowired
    private UserFreezeService userFreezeService;

    @Autowired
    private UsersService usersService;
    /**
     * 冻结
     *
     * @param userFreezeVo
     * @return
     */
    @PostMapping("freeze")
    public ResponseEntity<String> freeze(@RequestBody UserFreezeVo userFreezeVo) {

        Boolean bool = this.userFreezeService.freeze(userFreezeVo);

        if (bool) {
            return ResponseEntity.ok("操作成功");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    private static final ObjectMapper MAPPER = new ObjectMapper();

    /**
     * 解冻
     *
     * @param userFreezeVo
     * @return
     */
    @PostMapping("unfreeze")
    public ResponseEntity<String> unfreeze(@RequestBody UserFreezeVo userFreezeVo) {
        Boolean bool = this.userFreezeService.unfreeze(userFreezeVo);
        if (bool) {
            return ResponseEntity.ok("操作成功");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @GetMapping("{userId}")
    public ResponseEntity<Object> queryUserInfo(@PathVariable(name = "userId") Long userId) {
        UserVo userVo = this.usersService.queryUserInfo(userId);
        String jsonDate = null;
        try {
            jsonDate = MAPPER.writeValueAsString(userVo);
            return ResponseEntity.ok(jsonDate);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @GetMapping
    public PageResult<UserVo> queryByPage(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                          @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize,
                                          @RequestParam(value = "id") Long id,
                                          @RequestParam(value = "nickname") String nickname,
                                          @RequestParam(value = "city") String city) throws ParseException {
        return this.usersService.queryBypage(page, pageSize,id,nickname,city);
    }
}

