package com.tanhua.management.controller;


import com.tanhua.management.service.ManageCommentsService;
import com.tanhua.management.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("manage/messages")
public class ManageCommentsController {


    @Autowired
    private ManageCommentsService manageCommentsService;


    @GetMapping
    public ResponseEntity<PageResult> queryMomentsListById(@RequestParam(name="uid") Long uid,
                                                         @RequestParam(name = "pagesize") Integer pageSize,
                                                         @RequestParam(name = "page") Integer page,
                                                         @RequestParam(name = "sortProp" ) String sortProp,
                                                         @RequestParam(name = "sortOrder") String sortOrder){

        try {
            PageResult pageResult = this.manageCommentsService.queryMomentsListById(uid,page,pageSize,sortProp,sortOrder);
            if (null != pageResult){
                return ResponseEntity.ok(pageResult);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

    }



    @GetMapping("{messageID}")
    public ResponseEntity<PageResult> queryCommentsByMessageId(@RequestHeader(name = "Authorization") String token,
                                                                 @PathVariable(name="messageID") String messageID,
                                                               @RequestParam(name = "pagesize",defaultValue = "10") Integer pageSize,
                                                               @RequestParam(name = "page",defaultValue = "1") Integer page,
                                                               @RequestParam(name = "sortProp",required = false) String sortProp,
                                                               @RequestParam(name = "sortOrder",defaultValue = "descending ") String sortOrder){



        try {
            PageResult pageResult = this.manageCommentsService.queryCommentsListById(messageID,page,pageSize,sortProp,sortOrder);
            if (null != pageResult){
                return ResponseEntity.ok(pageResult);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

    }




}
