package com.tanhua.management.controller;

import com.tanhua.management.service.DashBoardService;
import com.tanhua.management.vo.InfoStatisticsVo;
import com.tanhua.management.vo.UsersStatisticsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("dashboard")
public class DashBoardController {

    @Autowired
    private DashBoardService dashBoardService;

    @GetMapping("summary")
    public InfoStatisticsVo InfoStatistics(){
        return dashBoardService.InfoStatistics();
    }
    @GetMapping("users")
    public UsersStatisticsVo getUsers(@RequestParam(value = "sd") Long sd,
                                      @RequestParam(value = "ed") Long ed,
                                      @RequestParam(value = "type") Integer type){
        return dashBoardService.getUsers(sd,ed,type);
    }

/*    @GetMapping("distribution")
    public void test2(){

    }*/
}
