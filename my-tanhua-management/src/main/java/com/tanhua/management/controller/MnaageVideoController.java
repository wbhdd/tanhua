package com.tanhua.management.controller;


import com.tanhua.management.service.ManageVideoService;
import com.tanhua.management.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("manage/videos")
public class MnaageVideoController {

    @Autowired
    private ManageVideoService manageVideoService;


    @GetMapping
    public ResponseEntity<PageResult> queryVideoListById(@RequestParam(name="uid") Long uid,
                                                         @RequestParam(name = "pagesize") Integer pageSize,
                                                         @RequestParam(name = "page") Integer page,
                                                         @RequestParam(name = "sortProp") String sortProp,
                                                         @RequestParam(name = "sortOrder") String sortOrder){

        try {
            if (page <= 0) {
                page = 1;
            }
            PageResult pageResult = this.manageVideoService.queryVideoListById(page, pageSize,uid,sortOrder,sortProp);
            if (null != pageResult) {
                return ResponseEntity.ok(pageResult);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

    }

}
