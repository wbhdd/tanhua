package com.tanhua.management.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;

/**
 * @ClassName :  UserMapper
 * @Author : Tenebrous
 * @Date: 2020/12/7 20:34
 * @Description : 枚举类
 */
public enum SexEnum implements IEnum<Integer> {

    /**
     *  男
     */
    MAN(1,"男"),
    /**
     *  女
     */
    WOMAN(2,"女"),
    /**
     *  性别未知
     */
    UNKNOWN(3,"未知");

    private int value;
    private String desc;

    SexEnum(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.desc;
    }
}