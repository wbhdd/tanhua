package com.tanhua.management.enums;

public enum LogsTypeEnum {
    /**
     * 操作类型,
     * 01为登录
     */
    LOGIN("01", "登录");

    private String value;
    private String name;

    LogsTypeEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
