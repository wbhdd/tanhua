package com.tanhua.management.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentsVo {

    private String id; //评论id
    private Long userId; //评论人id
    private String nickname; //昵称
    private String content; //评论
    private String createDate; //评论时间: 08:27
}
