package com.tanhua.management.vo;

import com.tanhua.management.enums.SexEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName :  UserVo
 * @Author : Tenebrous
 * @Date: 2020/12/23 10:41
 * @Description :
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserVo {

    /**
     * 用户ID
     */
    private long id;

    /**
     * 昵称
     */
    private String nickname;

    /**
     *手机号
     */
    private String mobile;

    /**
     *性别
     */
    private String sex;

    /**
     * 个性签名
     */
    private String personalSignature;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 被喜欢人数
     */
    private Long countBeLiked;

    /**
     * 喜欢人数
     */
    private Long countLiked;

    /**
     * 配对人数
     */
    private Long countMatching;

    /**
     * 收入
     */
    private Long income;

    /**
     * 职业,暂无该字段
     */
    private String occupation;

    /**
     * 用户状态，1为正常，2为冻结   TODO
     */
    private String userStatus;

    /**
     * 注册时间
     */
    private Long created;

    /**
     * 注册地区
     */
    private String city;

    /**
     * 最近活跃时间   TODO
     */
    private Long lastActiveTime;

    /**
     * 最近登录地   TODO
     */
    private String lastLoginLocation;

    /**
     * 头像
     */
    private String logo;

    /**
     * 用户标签
     */
    private String tags;
}
