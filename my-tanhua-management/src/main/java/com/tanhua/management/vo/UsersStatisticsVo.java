package com.tanhua.management.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsersStatisticsVo {
    /**
     * 本年
     */
    private List<DateVo> thisYear;
    /**
     * 去年
     */
    private List<DateVo> lastYear;
}
