package com.tanhua.management.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MomentsVo {

    private String id;
    private String nickname; //昵称
    private Long userId; //发布人id
    private Long createDate; //发布时间
    private List<String> medias;
    private String text;
    private Integer reportCount; //举报数量
    private Integer likeCount; //点赞数量
    private Integer commentCount; //评论数量
    private Integer forwardingCount; //转发数
}
