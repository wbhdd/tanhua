package com.tanhua.management.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @ClassName :  PageResult
 * @Author : Tenebrous
 * @Date: 2020/12/11 13:56
 * @Description : 推荐用户最终返回页面的结果对象
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageResult<T> {

    /**
     * 总记录数
     */
    private Integer counts;

    /**
     * 页大小
     */
    private Integer pageSize;

    /**
     * 总页数
     */
    private Integer pages;

    /**
     * 当前页码
     */
    private Integer page;

    /**
     * 推荐用户的信息集合
     * id、avatar、nickname、gender、age、tags、fateValue
     */
    private List<?> items;

}
