package com.tanhua.management.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author XDY
 * @date 2020/12/23
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminVo implements Serializable {

    private String uid;

    private String username;

    private String avatar;

}
