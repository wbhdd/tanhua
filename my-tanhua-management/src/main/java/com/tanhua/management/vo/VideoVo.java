package com.tanhua.management.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VideoVo {

    private String id;
    private String nickname; //昵称
    private Long userId; //发布人id
    private Long createDate; //发布时间
    private String picUrl; //封面URL
    private String videoUrl; //视频URL
    private Integer reportCount; //举报数量
    private Integer likeCount; //点赞数量
    private Integer commentCount; //评论数量
    private Integer forwardingCount; //转发数
}
