package com.tanhua.management;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName :  AdminLoginApplication
 * @Author : Tenebrous
 * @Date: 2020/12/22 19:06
 * @Description : 后台登录
 */
@MapperScan("com.tanhua.management.mapper")
@SpringBootApplication
public class AdminLoginApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminLoginApplication.class, args);
    }
}
