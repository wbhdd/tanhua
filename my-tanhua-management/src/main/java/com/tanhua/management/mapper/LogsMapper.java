package com.tanhua.management.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.management.pojo.Logs;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zwy
 * 日志接口
 */
@Mapper
public interface LogsMapper extends BaseMapper<Logs> {
}
