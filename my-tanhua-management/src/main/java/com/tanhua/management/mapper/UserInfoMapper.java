package com.tanhua.management.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.management.pojo.UserInfo;
import com.tanhua.management.vo.DataPointVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ClassName :  UserInfoMapper
 * @Author : Tenebrous
 * @Date: 2020/12/24 15:07
 * @Description : 从mysql中查找用户详细信息
 */
@Mapper
public interface UserInfoMapper extends BaseMapper<UserInfo> {

    List<DataPointVo> findIndustryDistribution();

    List<DataPointVo> findSexDistribution();
}
