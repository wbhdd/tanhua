package com.tanhua.management.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.management.pojo.Admin;

/**
 * @ClassName :  mapper
 * @Author : Tenebrous
 * @Date: 2020/12/22 23:27
 * @Description :
 */

public interface AdminMapper extends BaseMapper<Admin> {
}
