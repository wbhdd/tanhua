package com.tanhua.management.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.management.pojo.User;

/**
 * @ClassName :  UserMapper
 * @Author : Tenebrous
 * @Date: 2020/12/24 18:36
 * @Description :
 */
public interface UsersMapper extends BaseMapper<User> {
}

