package com.tanhua.management.msg;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tanhua.management.pojo.Log;
import com.tanhua.management.pojo.Logs;
import com.tanhua.management.service.LogsService;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * @author zwy
 * 消费者主题,消费者分组
 */
@Component
@RocketMQMessageListener(topic = "tanhua-log",
        consumerGroup = "tanhua-log-consumer")
public class LogsMsgConsumer implements RocketMQListener<String> {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    private LogsService logsService;

    // 模拟手机型号
    private static final String[] mockDevices = {
            "iphone20proMax", "iphone30proMax", "iphone40proMax", "iphone50proMax", "iphone60proMax", "iphone70proMax"
    };
    // 模拟操作地点
    private static final String[] mockCities = {
            "北京", "上海", "广州", "深圳", "合肥", "南京"
    };

    /**
     * 功能描述: onMessage
     *
     * @param message
     * @Return: void
     * @throws:
     * @Author: zwy
     * @Date: 2020/12/25 12:00
     */
    @Override
    public void onMessage(String message) {
        try {
            //解析字符串,JsonNode对象
            JsonNode jsonNode = MAPPER.readTree(message);
            //根据JsonNode对象获取到传入的数据
            long userId = jsonNode.get("userId").asLong();
            String type = jsonNode.get("type").asText();
            long date = jsonNode.get("date").asLong();
            //转换格式
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
            String logTime = simpleDateFormat.format(date);
            //如果有一个为空,就返回
            if (!ObjectUtil.isAllNotEmpty(userId, type, date)) {
                return;
            }

            //消息的校验
            //如果查出userId,type和date一样的数据的条数
            LambdaQueryWrapper<Logs> queryWrapper = Wrappers.<Logs>lambdaQuery()
                    .eq(Logs::getUserId, userId)
                    .eq(Logs::getType, type)
                    .eq(Logs::getLogTime, logTime);
            int count = this.logsService.count(queryWrapper);
            //如果存在的话就返回
            if (count > 0) {
                return;
            }
            Logs logs = new Logs();
            logs.setId(null);
            logs.setLogTime(logTime);
            logs.setUserId(userId);
            logs.setType(type);
            //随机将数据赋值给logs对象
            logs.setPlace(mockCities[RandomUtil.randomInt(0, mockCities.length - 1)]);
            logs.setEquipment(mockDevices[RandomUtil.randomInt(0, mockDevices.length - 1)]);
            //调用保存
            this.logsService.save(logs);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
