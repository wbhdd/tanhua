package com.tanhua.management.msg;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tanhua.management.enums.LogsTypeEnum;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zwy
 */
@Component
@RocketMQMessageListener(topic = "tanhua-sso-login",
        consumerGroup = "tanhua-sso-login-consumer")
public class LoginMsgConsumer implements RocketMQListener<String> {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    /**
     * 功能描述: onMessage   转发消息到  tanhua-log中
     *
     * @param message
     * @Return: void
     * @throws:
     * @Author: zwy
     * @Date: 2020/12/25 11:46
     */
    @Override
    public void onMessage(String message) {
        try {
            JsonNode jsonNode = MAPPER.readTree(message);

            Map<String, Object> logMsg = new HashMap<>();
            logMsg.put("userId", jsonNode.get("id").asLong());
            logMsg.put("type", LogsTypeEnum.LOGIN.getValue());
            logMsg.put("date", jsonNode.get("date").asText());

            //转发消息到  tanhua-log
            this.rocketMQTemplate.convertAndSend("tanhua-log", logMsg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
