package cn.itcast.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class WordCountApp2 {

    public static void main(String[] args) {

        // spark的配置
        SparkConf sparkConf = new SparkConf()
                .setAppName("WordCountApp2")
                .setMaster("local[*]"); //本地模式，并且使用和cpu的内核数相同的线程数进行执行

        // 定义上下文对象,它是程序的入口
        JavaSparkContext jsc = new JavaSparkContext(sparkConf);

        // 读取文件
        JavaRDD<String> fileRdd = jsc.textFile("E:\\vue\\github\\tanhua222\\tanhua\\data");

        // 单词计数的逻辑处理
        List<Tuple2<String, Integer>> list = fileRdd.flatMap(s -> {
            String[] ss = s.split(" ");
            return Arrays.asList(ss).iterator();
        }).mapToPair(s -> new Tuple2<>(s, 1))
                .reduceByKey((v1, v2) -> v1 + v2)
                .collect();


        // 遍历输出
        list.forEach(obj -> System.out.println(obj._1() + "出现的次数为：" + obj._2()));


        // 关闭，释放资源
        jsc.stop();
    }
}
