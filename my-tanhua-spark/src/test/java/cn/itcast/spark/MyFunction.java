package cn.itcast.spark;

import org.apache.spark.api.java.function.Function;

public class MyFunction implements Function<Integer, Integer> {

    private static final long serialVersionUID = 8375210480848027494L;

    @Override
    public Integer call(Integer v1) throws Exception {
        return v1 * 2;
    }
}
