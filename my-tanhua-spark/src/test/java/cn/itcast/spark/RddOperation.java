package cn.itcast.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.Test;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RddOperation {

    @Test
    public void testMap() {
        SparkConf sparkConf = new SparkConf()
                .setAppName("RddOperation")
                .setMaster("local[*]");
        JavaSparkContext jsc = new JavaSparkContext(sparkConf);

        // 生成一个测试rdd数据集
        JavaRDD<Integer> rdd1 = jsc.parallelize(Arrays.asList(1, 2, 3, 4, 5));

        // 将rdd1中每一个元素都乘以2，生成新的rdd
//        JavaRDD<Integer> rdd2 = rdd1.map(new MyFunction()); 方案一
        JavaRDD<Integer> rdd2 = rdd1.map(v1 -> v1 * 2); //方案二

        List<Integer> list = rdd2.collect();
        list.forEach(integer -> System.out.println(integer));

        jsc.stop();
    }

    @Test
    public void testFilter(){
        SparkConf sparkConf = new SparkConf()
                .setAppName("RddOperation")
                .setMaster("local[*]");
        JavaSparkContext jsc = new JavaSparkContext(sparkConf);

        // 生成一个测试rdd数据集
        JavaRDD<Integer> rdd1 = jsc.parallelize(Arrays.asList(1, 2, 3, 4, 5));

        // 过滤数据，用大于2的数据组成一个新的rdd
        JavaRDD<Integer> rdd2 = rdd1.filter(v1 -> v1 > 2);

        List<Integer> list = rdd2.collect();

        list.forEach(integer -> System.out.println(integer));

        jsc.stop();
    }

    @Test
    public void testFlatMap() {
        SparkConf sparkConf = new SparkConf()
                .setAppName("RddOperation")
                .setMaster("local[*]");
        JavaSparkContext jsc = new JavaSparkContext(sparkConf);
        // 生成RDD
        JavaRDD<String> rdd = jsc.parallelize(Arrays.asList("hello world","hello spark"));
        // 数据压扁操作
        JavaRDD<String> rdd2 = rdd.flatMap(s -> Arrays.asList(s.split(" ")).iterator());

        //收集数据
        List<String> list = rdd2.collect();
        for (String o : list) {
            System.out.println(o);
        }
    }

    @Test
    public void testMapPartitions (){
        SparkConf sparkConf = new SparkConf()
                .setAppName("RddOperation")
                .setMaster("local[*]");
        JavaSparkContext jsc = new JavaSparkContext(sparkConf);
        // 生成RDD，分3个区存储
        JavaRDD<Integer> rdd = jsc.parallelize(Arrays.asList(1,2,3,4,5,6,7,8,9,10),3);
        // 分区操作数据
        JavaRDD<Integer> rdd2 = rdd.mapPartitions(integerIterator -> {
            List<Integer> result = new ArrayList<>();
            while (integerIterator.hasNext()){
                Integer i = integerIterator.next();
                result.add(i);
            }
            System.out.println("分区数据：" + result);
            return result.iterator();
        });

        //收集数据
        List<Integer> list = rdd2.collect();
        list.forEach(integer ->  System.out.println(integer));
    }

    @Test
    public void testMapToPair (){
        SparkConf sparkConf = new SparkConf()
                .setAppName("RddOperation")
                .setMaster("local[*]");
        JavaSparkContext jsc = new JavaSparkContext(sparkConf);
        // 生成RDD
        JavaRDD<String> rdd = jsc.parallelize(Arrays.asList("hello world","hello spark"));

        // 按照空格分割
        JavaRDD<String> rdd1 = rdd.flatMap(s -> Arrays.asList(s.split(" ")).iterator());

        // 把每一个单词的出现数量标记为1
        JavaPairRDD<Object, Object> rdd2 = rdd1.mapToPair(s -> new Tuple2<>(s, 1));

        //收集数据
        List<Tuple2<Object, Object>> list = rdd2.collect();
        for (Tuple2<Object, Object> o : list) {
            System.out.println(o);
        }
    }

    @Test
    public void testReduceByKey (){
        SparkConf sparkConf = new SparkConf()
                .setAppName("RddOperation")
                .setMaster("local[*]");
        JavaSparkContext jsc = new JavaSparkContext(sparkConf);
        // 生成RDD
        JavaRDD<String> rdd = jsc.parallelize(Arrays.asList("hello world","hello spark"));

        // 按照空格分割
        JavaRDD<String> rdd1 = rdd.flatMap(s -> Arrays.asList(s.split(" ")).iterator());

        // 把每一个单词的出现数量标记为1
        JavaPairRDD<Object, Integer> rdd2 = rdd1.mapToPair(s -> new Tuple2<>(s, 1));

        // 按照key相同的数进行相加操作
        JavaPairRDD<Object, Integer> rdd3 = rdd2.reduceByKey((v1, v2) -> v1 + v2);

        //收集数据
        List<Tuple2<Object, Integer>> list = rdd3.collect();
        for (Tuple2<Object, Integer> o : list) {
            System.out.println(o);
        }
    }

    @Test
    public void testCoalesce (){
        SparkConf sparkConf = new SparkConf()
                .setAppName("RddOperation")
                .setMaster("local[*]");
        JavaSparkContext jsc = new JavaSparkContext(sparkConf);
        // 生成RDD
        JavaRDD<Integer> rdd = jsc.parallelize(Arrays.asList(1,2,3,4,5,6,7,8,9,10));

        System.out.println(rdd.getNumPartitions()); //4
        // 重新分区，shuffle：是否重新分配存储，如果分区数大于原有分区数，就需要设置为true
        JavaRDD<Integer> rdd2 = rdd.coalesce(6, true);
        System.out.println(rdd2.getNumPartitions()); //2

        //收集数据
        List<Integer> list = rdd2.collect();
        for (Integer o : list) {
            System.out.println(o);
        }
    }


}
