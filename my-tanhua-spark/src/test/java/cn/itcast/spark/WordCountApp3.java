package cn.itcast.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * 打包到集群上运行
 */
public class WordCountApp3 {

    public static void main(String[] args) {
        //打包到服务器上
//        SparkConf sparkConf = new SparkConf()
//                .setAppName("WordCountApp");
//              //  .setMaster("local[*]");
        //本地运行 spark的配置
        SparkConf sparkConf = new SparkConf()
                .setAppName("WordCountApp")
                .setMaster("local[*]"); //本地模式，并且使用和cpu的内核数相同的线程数进行执行，如果是到服务器就注销这个

        // 定义上下文对象,它是程序的入口
        JavaSparkContext jsc = new JavaSparkContext(sparkConf);
/**
 * ./spark-submit  --master spark://node2:7077  --class
 cn.itcast.spark.WordCountApp /itcast/my-spark-test-1.0-SNAPSHOT.jar
   /itcast/word.txt   /itcast/wc
 /itcast/word.txt(数据读取地址)   /itcast/wc(数据输出地址)
 */
        // 读取文件  args[0]：数据输入的目录  使用命令这么做
       // JavaRDD<String> fileRdd = jsc.textFile(args[0]);
        JavaRDD<String> fileRdd = jsc.textFile("E:\\vue\\github\\tanhua222\\tanhua\\data");
        // 压扁操作，并且按照空格分割
        JavaRDD<String> flatMapRdd = fileRdd.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public Iterator<String> call(String s) throws Exception {
                String[] ss = s.split(" ");
                return Arrays.asList(ss).iterator();
            }
        });

        // 对单词做计数
        JavaPairRDD<Object, Integer> mapToPairRdd = flatMapRdd.mapToPair(new PairFunction<String, Object, Integer>() {
            @Override
            public Tuple2<Object, Integer> call(String s) throws Exception {
                return new Tuple2<>(s, 1);
            }
        });

        // 对相同的单词做相加操作
        JavaPairRDD<Object, Integer> reduceByKeyRdd = mapToPairRdd.reduceByKey(new Function2<Integer, Integer, Integer>() {
            @Override
            public Integer call(Integer v1, Integer v2) throws Exception {
                return v1 + v2;
            }
        });


        // 执行计算  collect():触发任务执行，收集结果数据
        List<Tuple2<Object, Integer>> collect = reduceByKeyRdd.collect();
        for (Tuple2<Object, Integer> obj : collect) {
            System.out.println(obj._1() + "===出现的次数为====："+ obj._2());
        }

        // 保存结果到指定的目录或文件  args[1]输出的目录   这个要想在本地执行成功的话，需要spark-3.0.0-bin-hadoop3.2依赖在全局环境中
        //   reduceByKeyRdd.saveAsTextFile(args[1]);
        reduceByKeyRdd.saveAsTextFile("E:\\vue\\github\\tanhua222\\tanhua\\result\\");

        // 关闭，释放资源
        jsc.stop();
    }
}
