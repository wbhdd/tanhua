package cn.itcast.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Int;
import scala.Tuple2;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 sc ：Spark-Shell 中已经默认将SparkContext 类初始化为对象sc。用户代码如果需要用到，则直接
 应用sc 即可。
 textFile ：读取数据文件，file:// 读取本地文件
 flatMap ：对文件中的每一行数据进行压平切分,这里按照空格分隔。
 map ：对出现的每一个单词记为1（word，1）
 reduceByKey ：对相同的单词出现的次数进行累加
 collect ：触发任务执行，收集结果数据。
 */
public class WordCountApp {

    public static void main(String[] args) {

        // spark的配置
        SparkConf sparkConf = new SparkConf()
                .setAppName("WordCountApp")   //AppName就是应用的名称：WordCountApp
                .setMaster("local[*]"); //本地模式，并且使用和cpu的内核数相同的线程数进行执行，如果到服务器上的话，就注销这个
        //看WordCountApp3对.setMaster("local[*]"); 的操作
        //setMaster("local[2]");表示当前应用使用两个线程执行，如果是*表示读取cpu的内核数，并相同的线程数进行执行

        // 定义上下文对象,它是程序的入口
        JavaSparkContext jsc = new JavaSparkContext(sparkConf);

        // 读取文件
        JavaRDD<String> fileRdd = jsc.textFile("E:\\vue\\github\\tanhua222\\tanhua\\data");

        // 压扁操作，并且按照空格分割
        //操作都是字符串，所以new FlatMapFunction里面我们写成字符串
        JavaRDD<String> flatMapRdd = fileRdd.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public Iterator<String> call(String s) throws Exception {
                //根据空格隔开
                String[] ss = s.split(" ");
                //通过数组获取迭代器
                return Arrays.asList(ss).iterator();
            }
        });


        // 对单词做计数  flatMapRdd.mapToPair形成k-v这样的数据结构，给每个单词都弄成(key,1),这里对相同的单词不进行累加的操作
        // (中国,1),(世界,1),(宇宙,1),
        // (浩瀚,1),(中国,1),(宇宙,1)
        JavaPairRDD<Object, Integer> mapToPairRdd = flatMapRdd.mapToPair(new PairFunction<String, Object, Integer>() {
          //第一个表示单词，第二个表示单词计数的数字
            @Override
            public Tuple2<Object, Integer> call(String s) throws Exception {
                //第一个单词s:单词的意思，第二个1表示1,,不会对相同的单词进行累加操作
                //换句话说就是为每个表示进行数字标记为1，形成
                // (中国,1),(世界,1),(宇宙,1),
                // (浩瀚,1),(中国,1),(宇宙,1)
                return new Tuple2<>(s, 1);
            }
        });

        // 对相同的单词(key)做相加操作
        // (中国,1),(世界,1),(宇宙,1),
        // (浩瀚,1),(中国,1),(宇宙,1)
        //转变为(中国,2),(世界,1),(宇宙,2),(浩瀚,1)
        JavaPairRDD<Object, Integer> reduceByKeyRdd = mapToPairRdd.reduceByKey(new Function2<Integer, Integer, Integer>() {
            @Override
            public Integer call(Integer v1, Integer v2) throws Exception {
                return v1 + v2;
            }
        });

        // 执行计算  collect():触发任务执行，收集结果数据
        List<Tuple2<Object, Integer>> collect = reduceByKeyRdd.collect();
        for (Tuple2<Object, Integer> obj : collect) {
            System.out.println(obj._1() + "===出现的次数为====："+ obj._2());
        }


        // 关闭，释放资源
        jsc.stop();
    }
}
