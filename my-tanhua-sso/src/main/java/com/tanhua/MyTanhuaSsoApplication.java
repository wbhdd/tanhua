package com.tanhua;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;

/**
 * @ClassName :  UserMapper
 * @author Tenebrous  : Tenebrous
 * @Date: 2020/12/7 20:34
 * @Description : Springboot的启动类
 */
@MapperScan("com.tanhua.sso.mapper")
@SpringBootApplication(exclude = {MongoAutoConfiguration.class})
public class MyTanhuaSsoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyTanhuaSsoApplication.class, args);
    }
}
