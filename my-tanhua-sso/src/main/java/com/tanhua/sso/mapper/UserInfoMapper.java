package com.tanhua.sso.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.sso.pojo.UserInfo;

/**
 * @ClassName :  UserMapper
 * @Author : Tenebrous
 * @Date: 2020/12/7 20:34
 * @Description : 用户信息接口
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}