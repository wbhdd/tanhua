package com.tanhua.sso.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.sso.pojo.UserFreeze;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author XDY
 * @date 2020/12/24
 */
@Mapper
public interface UserFreezeMapper extends BaseMapper<UserFreeze> {
}
