package com.tanhua.sso.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.sso.mapper.UserFreezeMapper;
import com.tanhua.sso.pojo.UserFreeze;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;


/**
 * @author XDY
 * @date 2020/12/24
 */
@Service
public class UserFreezeService {

    private static final String CACHE_KEY_FREEZE_PREFIX = "FREEZE_";

    @Autowired
    private UserFreezeMapper userFreezeMapper;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 查询冻结范围
     * @param userId
     * @return
     */
    public Integer queryFreezingRange(Long userId) {
        try {
            String cacheKey = CACHE_KEY_FREEZE_PREFIX + userId;
            String state = this.redisTemplate.opsForValue().get(cacheKey);
            if (StringUtils.isNotEmpty(state)){
                // 创建查询条件构建器
                QueryWrapper<UserFreeze> queryWrapper = new QueryWrapper<>();
                // 设置条件
                queryWrapper.eq("user_id", userId);
                UserFreeze oneUserFreeze = this.userFreezeMapper.selectOne(queryWrapper);
                return oneUserFreeze.getFreezingRange();
        }
            userFreezeMapper.delete(new QueryWrapper<UserFreeze>().eq("user_id", userId));
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
