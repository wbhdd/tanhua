package com.tanhua.sso.service;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tanhua.sso.config.HuanXinConfig;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName :  HuanxinTokenService
 * @Author : Tenebrous
 * @Date: 2020/12/16 11:21
 * @Description : 环信用户系统集成之获取管理员权限
 */
@Service
public class HuanXinTokenService {
    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    private HuanXinConfig huanXinConfig;

    @Autowired
    private RestTemplate restTemplate;

    private static final String REDIS_KEY = "HUANXIN_TOKEN";

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    public String getToken() {
        // 先从Redis中命中
        String cacheData = this.redisTemplate.opsForValue().get(REDIS_KEY);
        if (StringUtils.isNotEmpty(cacheData)) {
            return cacheData;
        }
        String targetUrl = this.huanXinConfig.getUrl()
                + this.huanXinConfig.getOrgName() + "/" +
                this.huanXinConfig.getAppName() + "/token";

        Map<String, Object> param = new HashMap<>(3);
        param.put("grant_type", "client_credentials");
        param.put("client_id", this.huanXinConfig.getClientId());
        param.put("client_secret", this.huanXinConfig.getClientSecret());

        //请求环信接口
        ResponseEntity<String> responseEntity =
                this.restTemplate.postForEntity(targetUrl, param, String.class);

        int code = 200;
        if (responseEntity.getStatusCodeValue() != code) {
            return null;
        }
        String body = responseEntity.getBody();
        try {
            JsonNode jsonNode = MAPPER.readTree(body);
            String accessToken = jsonNode.get("access_token").asText();
            // 过期时间提前一条天失效
            long expiresIn = jsonNode.get("expires_in").asLong() - 86400;
            if (StringUtils.isNotBlank(accessToken)) {
                // 将token保存到redis，有效期为5天，环信接口返回的有效期为6天
                this.redisTemplate.opsForValue().set(REDIS_KEY, accessToken, expiresIn, TimeUnit.SECONDS);
                return accessToken;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
