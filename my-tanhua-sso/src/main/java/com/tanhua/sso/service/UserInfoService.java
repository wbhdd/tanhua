package com.tanhua.sso.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.sso.enums.SexEnum;
import com.tanhua.sso.mapper.UserInfoMapper;
import com.tanhua.sso.pojo.User;
import com.tanhua.sso.pojo.UserInfo;
import com.tanhua.sso.vo.PicUploadResult;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;
/**
 * @ClassName :  UserInfoService
 * @Author : Tenebrous
 * @Date: 2020/12/7 20:34
 * @Description : 用户信息业务层
 */
@Service
public class UserInfoService {

    /**
     * 日志
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(UserInfoService.class);

    /**
     * userInfo的持久层接口
     */
    @Autowired
    private UserInfoMapper userInfoMapper;

    /**
     * userService接口
     */
    @Autowired
    private UserService userService;

    /**
     *  人脸识别引擎
     */
    @Autowired
    private FaceEngineService faceEngineService;

    /**
     * 图片上传业务层
     */
    @Autowired
    private PicUploadService picUploadService;

    /**
     *第一次登录注册时，需要完善个人信息
     * @param param  注册完以后完善信息所填写的信息(昵称、生日、城市)，封装成了map集合 param
     * @param token 用户token
     * @return   用户存在true|用户不存在false
     */
    public Boolean saveUserInfo(Map<String, String> param, String token) {
        // 根据用户的token获取user信息
        User user = this.userService.queryUserByToken(token);
        if (user == null) {
            // 如果为空，返回false
            return false;
        }
        // 创建userInfo对象
        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(user.getId());
        // 设置用户性别
        userInfo.setSex(StringUtils.equals(param.get("gender"), "man") ? SexEnum.MAN : SexEnum.WOMAN);
        userInfo.setNickName(param.get("nickname"));
        userInfo.setBirthday(param.get("birthday"));
        userInfo.setCity(param.get("city"));

        // 保存UserInfo数据到数据库
        this.userInfoMapper.insert(userInfo);
        return true;
    }

    /**
     * 保存头像
     * @param file      头像图片文件
     * @param token     用户token
     * @return          用户存在true|用户不存在false
     */
    public Boolean saveLogo(MultipartFile file, String token) {
        // 根据用户的token获取user信息
        User user = this.userService.queryUserByToken(token);
        if (user == null) {
            // 如果为空，返回false
            return false;
        }
        try {
            //校验头像是否为人像
            boolean isPortrait = this.faceEngineService.checkIsPortrait(file.getBytes());
            if (!isPortrait) {
                return false;
            }
        } catch (Exception e) {
            LOGGER.error("检测人像图片出错!", e);
            return false;
        }
        // 图片上传到阿里云OSS
        PicUploadResult uploadResult = this.picUploadService.upload(file);

        UserInfo userInfo = new UserInfo();
        userInfo.setLogo(uploadResult.getName());

        QueryWrapper<UserInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", user.getId());

        this.userInfoMapper.update(userInfo, queryWrapper);

        return true;
    }
}