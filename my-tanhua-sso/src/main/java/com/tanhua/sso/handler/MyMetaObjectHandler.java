package com.tanhua.sso.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @ClassName :  MyMetaObjectHandler
 * @Author : Tenebrous
 * @Date: 2020/12/7 20:26
 * @Description : 创建和更新日期自动填充实现
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    /**
     * 创建数据时自动填充时间
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        Object created = getFieldValByName("created", metaObject);
        if (null == created) {
            // 字段为空，可以进行填充
            setFieldValByName("created", new Date(), metaObject);
        }
        Object updated = getFieldValByName("updated", metaObject);
        if (null == updated) {
            // 更新字段为空，可以填充
            setFieldValByName("updated", new Date(), metaObject);
        }
    }

    /**
     *  更新数据时自动填充
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        setFieldValByName("updated", new Date(), metaObject);
    }
}
