package com.tanhua.sso.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @ClassName :  UserMapper
 * @Author : Tenebrous
 * @Date: 2020/12/7 20:34
 * @Description : user类，封装用户id，手机号、密码
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User extends BasePojo {

    /**
     *  用户id
     */
    private Long id;
    /**
     * 手机号
     */
    private String mobile;

    /**
     * 密码，json序列化时忽略
     */
    @JsonIgnore
    private String password;

}