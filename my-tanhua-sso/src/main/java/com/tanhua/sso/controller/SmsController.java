package com.tanhua.sso.controller;

import com.tanhua.sso.service.SmsService;
import com.tanhua.sso.vo.ErrorResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;

/**
 * @ClassName :  SmsController
 * @Author : Tenebrous
 * @Date: 2020/12/8 10:25
 * @Description : 短信登录的表现层
 */

@RestController
@RequestMapping("user")
public class SmsController {

    @Autowired
    private SmsService smsService;

    /**
     * 短信注册
     * @param param 前端传来的手机号，用json反序列化为一个map
     * @return 响应状态码，返回ResponseEntity
     */
    @PostMapping("login")
    public ResponseEntity<Object> sendCheckCode(@RequestBody Map<String, Object> param) {
        ErrorResult.ErrorResultBuilder builder = ErrorResult.builder().errCode("000000").errMessage("短信发送失败");
        // 拿到前端传来的手机号，转成String类型
        String phone = String.valueOf(param.get("phone"));
        // 传到service层，返回状态码和状态信息
        Map<String, Object> sendCheckCode = this.smsService.sendCheckCode(phone);
        int code = (Integer) (sendCheckCode.get("code"));
        if (code == 3) {
            return ResponseEntity.ok(null);
        } else if (code == 1) {
            // 发送失败，上一次发送的验证码还未失效
            String msg = sendCheckCode.get("msg").toString();
            builder.errCode("000001").errMessage(msg);
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(builder.build());
    }
}
