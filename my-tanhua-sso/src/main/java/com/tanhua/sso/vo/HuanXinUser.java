package com.tanhua.sso.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName :  HuanXinUser
 * @Author : Tenebrous
 * @Date: 2020/12/16 11:27
 * @Description : 环信用户实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HuanXinUser {

    private String username;
    private String password;
}
