package com.tanhua.sso.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName :  UserMapper
 * @Author : Tenebrous
 * @Date: 2020/12/7 20:34
 * @Description : 返回错误结果
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResult {

    /**
     * 错误状态码
     */
    private String errCode;

    /**
     * 错误信息
     */
    private String errMessage;
}
