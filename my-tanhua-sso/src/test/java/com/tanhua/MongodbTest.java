package com.tanhua;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.function.Consumer;

import static com.mongodb.client.model.Filters.eq;

/**
 * @ClassName :  MongodbTest
 * @Author : Tenebrous
 * @Date: 2020/12/9 21:22
 * @Description :
 */

public class MongodbTest {

    private MongoCollection<Document> collection;
    private MongoClient mongoClient;

    @Before
    public void init() {
        // 建立连接
        mongoClient = MongoClients.create("mongodb://192.168.45.129:27017");

        // 选择数据库
        MongoDatabase database = mongoClient.getDatabase("testdb");

        // 选择表
        collection = database.getCollection("user");
    }

    @After
    public void testClose() {
        mongoClient.close();
    }

    /**
     * 查询age<=50并且id>=100的用户信息，并且按照id倒序排序，只返回id，age字段，不返回_id字段
     */
    @Test
    public void testQuery() {
        this.collection.find(
                Filters.and(
                        Filters.lte("age", 50),
                        Filters.gte("id", 100)
                )
        ).sort(Sorts.descending("id"))
                .projection(Projections.fields(
                        Projections.include("id", "age"),
                        // 排除_id字段
                        Projections.excludeId()
                ))
                .forEach(
                (Consumer<? super Document>) document
                        ->
                        System.out.println(document.toJson())
        );
    }

    @Test
    public void testInsert() {

        // 添加数据
        Document document = new Document();
        document.append("id", 9999);
        document.append("username", "张三");
        document.append("age", 30);
        this.collection.insertOne(document);

        // 查询数据
        this.collection.find(eq("id", 9999)).forEach(
                (Consumer<? super Document>) document1
                        ->
                        System.out.println(document1.toJson())
        );
    }

    @Test
    public void testUpdate() {
        // 更新数据
        UpdateResult result = this.collection.updateOne(eq("id", 9999), Updates.set("age", 35));

        // 查询数据
        this.collection.find(eq("id", 9999)).forEach(
                (Consumer<? super Document>) document1
                        ->
                        System.out.println(document1.toJson())
        );

        System.out.println(result);
    }

    @Test
    public void testDelete() {
        // 删除数据
        DeleteResult result = this.collection.deleteMany(eq("age", 25));
        System.out.println(result);

    }
}
