package com.tanhua;

import org.apache.commons.lang3.RandomUtils;

/**
 * @ClassName :  Test
 * @Author : Tenebrous
 * @Date: 2020/12/10 10:39
 * @Description : 1
 */

public class Test {

    public static void main(String[] args) {
        System.out.println("INSERT INTO `tb_user_info` (`id`, `user_id`, `nick_name`, `logo`, `tags`, `sex`, `age`, `edu`, `city`, `birthday`, `cover_pic`, `industry`, `income`, `marriage`, `created`, `updated`) VALUES ('1', '1', 'heima', 'https://zytanhua.oss-cn-hangzhou.aliyuncs.com/images/logo/21.jpg', '单身,本科,年龄相仿', '1', '30', '本科', '北京市-北京城区-东城区', '2019-08-01', 'https://itcast-tanhua.oss-cn-shanghai.aliyuncs.com/images/logo/21.jpg', '计算机行业', '30', '已婚', '2019-08-02 16:44:23', '2019-08-02 16:44:23');");
        System.out.println("INSERT INTO `tb_user_info` (`id`, `user_id`, `nick_name`, `logo`, `tags`, `sex`, `age`, `edu`, `city`, `birthday`, `cover_pic`, `industry`, `income`, `marriage`, `created`, `updated`) VALUES ('2', '2', 'heima_2', 'https:zytanhua.oss-cn-hangzhou.aliyuncs.com/images/logo/22.jpg', '单身,本科,年龄相仿', '1', '30', '本科', '北京市-北京城区-东城区', '2019-08-01', 'https://itcast-tanhua.oss-cn-shanghai.aliyuncs.com/images/logo/22.jpg', '计算机行业', '30', '已婚', '2019-08-02 16:44:23', '2019-08-02 16:44:23');");
        for (int i = 3; i < 100; i++) {
            String logo = "https://zytanhua.oss-cn-hangzhou.aliyuncs.com/images/logo/"+ RandomUtils.nextInt(1,20)+".jpg";
            System.out.println("INSERT INTO `tb_user_info` (`id`, `user_id`, `nick_name`, `logo`, `tags`, `sex`, `age`, `edu`, `city`, `birthday`, `cover_pic`, `industry`, `income`, `marriage`, `created`, `updated`) VALUES ('"+i+"', '"+i+"', 'heima_"+i+"', '"+logo+"', '单身,本科,年龄相仿', '1', '"+RandomUtils.nextInt(20,50)+"', '本科', '北京市-北京城区-东城区', '2019-08-01', '"+logo+"', '计算机行业', '40', '未婚', '2019-08-02 16:44:23', '2019-08-02 16:44:23');");
        }
    }
}
