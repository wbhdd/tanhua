package com.tanhua;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.function.Consumer;

/**
 * @ClassName :  TestMongodb
 * @Author : Tenebrous
 * @Date: 2020/12/9 21:12
 * @Description : mongodb测试
 */
public class TestMongodb {

    public static void main(String[] args) {
        // 建立连接
        MongoClient mongoClient = MongoClients.create("mongodb://192.168.45.129:27017");

        // 选择数据库
        MongoDatabase database = mongoClient.getDatabase("testdb");

        // 选择表
        MongoCollection<Document> userCollection = database.getCollection("user");

        // 查询数据
        userCollection.find().limit(100).forEach(
                (Consumer<? super Document>) document
                        ->
                        System.out.println(document.toJson())
        );

        // 关闭连接
        mongoClient.close();
    }

}
