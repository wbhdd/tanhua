package com.tanhua;

import com.tanhua.sso.service.FaceEngineService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class TestFaceEngineService {

    @Autowired
    private FaceEngineService faceEngineService;

    @Test
    public void testCheckIsPortrait(){
//        File file = new File("D:\\PrepationDocument\\tanhua\\day01\\资料\\测试图片\\1.jpg");
        File file = new File("C:\\Users\\Tenebrous\\Desktop\\探花-前9天资料\\day01-项目介绍以及实现登录功能\\资料\\测试图片\\1.jpg");
        boolean checkIsPortrait = this.faceEngineService.checkIsPortrait(file);
        System.out.println(checkIsPortrait); // true|false
    }
}
