package com.tanhua;

import com.tanhua.sso.service.SmsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Map;

/**
 * @ClassName :  TestSmsService
 * @Author : Tenebrous
 * @Date: 2020/12/7 21:44
 * @Description : 11
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestSmsService {

    @Autowired
    private SmsService smsService;

    @Test
    public void testSend() {
        String s = this.smsService.sendSmsAliyun("17353766007");
        System.out.println(s);
    }
}
