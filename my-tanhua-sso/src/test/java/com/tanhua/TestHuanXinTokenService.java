package com.tanhua;

import com.tanhua.sso.service.HuanXinTokenService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestHuanXinTokenService {

    @Autowired
    private HuanXinTokenService huanxinTokenService;

    @Test
    public void testGetToken(){
        String token = this.huanxinTokenService.getToken();
        System.out.println(token);
    }

}